import {FETCH_ALL_MESSAGES, FETCH_NEW_MESSAGE} from "../actions/actionTypes";

const initialState = {
  messages: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_NEW_MESSAGE:
      return {...state, messages: state.messages.concat(action.message)};
    case FETCH_ALL_MESSAGES:
      return {...state, messages: action.messages};
    default:
      return state;
  }
};

export default reducer;