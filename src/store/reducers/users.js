import {
  CONNECTED_USERS,
  LOGIN_USER_FAILURE, LOGIN_USER_SUCCESS, LOGOUT_USER, REGISTER_USER_FAILURE,
  REGISTER_USER_SUCCESS, USER_LOGGED_IN, USER_LOGGED_OUT
} from "../actions/actionTypes";

const initialState = {
  registerError: null,
  loginError: null,
  user: null,
  users: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_USER_SUCCESS:
      return {...state, registerError: null};
    case REGISTER_USER_FAILURE:
      return {...state, registerError: action.error};
    case LOGIN_USER_SUCCESS:
      return {...state, user: action.user, loginError: null};
    case LOGIN_USER_FAILURE:
      return {...state, loginError: action.error};
    case LOGOUT_USER:
      return {...state, user: null};
    case USER_LOGGED_IN:
      let users = [...state.users];
      users.push(action.user);
      return {...state, users};
    case USER_LOGGED_OUT:
      return {...state, users: action.users};
    case CONNECTED_USERS:
      return {...state, users: action.users};
    default:
      return state;
  }
};

export default reducer;