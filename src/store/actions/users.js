import axios from '../../axios-api';
import {push} from 'react-router-redux';
import {NotificationManager} from 'react-notifications';
import {
  CONNECTED_USERS,
  LOGIN_USER_FAILURE, LOGIN_USER_SUCCESS, LOGOUT_USER, REGISTER_USER_FAILURE,
  REGISTER_USER_SUCCESS, USER_LOGGED_IN, USER_LOGGED_OUT
} from "./actionTypes";

const registerUserSuccess = () => {
  return {type: REGISTER_USER_SUCCESS};
};

const registerUserFailure = error => {
  return {type: REGISTER_USER_FAILURE, error};
};

export const registerUser = userData => {
  return dispatch => {
    return axios.post('/users', userData).then(
      () => {
        dispatch(registerUserSuccess());
        dispatch(push('/'));
        NotificationManager.success('Success', 'Registration successful');
      },
      error => {
        dispatch(registerUserFailure(error.response.data));
      }
    );
  };
};

const loginUserSuccess = (user) => {
  return {type: LOGIN_USER_SUCCESS, user};
};

const loginUserFailure = (error) => {
  return {type: LOGIN_USER_FAILURE, error};
};

export const loginUser = userData => dispatch => {
  return axios.post('/users/sessions', userData)
    .then(res => {
        dispatch(loginUserSuccess(res.data));
        dispatch(push('/'));
        NotificationManager.success('Success', res.data.message);
      },
      error => {
        const err = error.response ? error.response.data : {error: 'No internet connection'};
        dispatch(loginUserFailure(err));
      });
};

export const logoutUser = () => (dispatch) => {
  axios.delete('/users/sessions')
    .then(res => {
      dispatch(push('/login'));
      dispatch({type: LOGOUT_USER});
      NotificationManager.success('Success', res.data.message);
    },
    error => console.log('Logout error')
  )
};

export const logoutExpiredUser = () => {
  return dispatch => {
    dispatch({type: LOGOUT_USER});
    dispatch(push('/login'));
    NotificationManager.error('Error', 'Your session has expired, please login again');
  }
};

export const loggedIn = user => ({
  type: USER_LOGGED_IN, user
});

export const loggedOut = (users) => ({
  type: USER_LOGGED_OUT, users
});

export const fetchConnectedUsers = users => ({
  type: CONNECTED_USERS, users
});
