import {FETCH_ALL_MESSAGES, FETCH_NEW_MESSAGE} from "./actionTypes";

export const fetchNewMessage = message => {
  return {type: FETCH_NEW_MESSAGE, message};
};

export const fetchAllMessages = messages => {
  return {type: FETCH_ALL_MESSAGES, messages};
};
