import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Col, FormControl, FormGroup, Panel, Row} from "react-bootstrap";
import {fetchAllMessages, fetchNewMessage} from "../../store/actions/messages";
import {CONNECTED_USERS, USER_LOGGED_IN, USER_LOGGED_OUT} from "../../store/actions/actionTypes";
import {fetchConnectedUsers, loggedIn, loggedOut} from "../../store/actions/users";

class Chat extends Component {

  state = {
    message: '',
    sender: null,
    isPrivateMessage: false
  };

  componentWillUnmount() {
    this.websocket && this.websocket.close();
  };

  start = (token) => {
    this.websocket = new WebSocket(`ws://localhost:8000/chat?token=${token}`);

    this.websocket.onmessage = (message) => {
      const decodedMessage = JSON.parse(message.data);

      switch (decodedMessage.type) {
        case USER_LOGGED_IN:
          this.props.onLoggedIn(decodedMessage.user);
          break;
        case USER_LOGGED_OUT:
          this.props.onLoggedOut(decodedMessage.connectedUsers);
          break;
        case CONNECTED_USERS:
          this.props.onFetchConnectedUsers(decodedMessage.connectedUsers);
          break;
        case 'NEW_MESSAGE':
          this.props.onFetchNewMessage(decodedMessage.message);
          break;
        case 'ALL_MESSAGES':
          this.props.onFetchAllMessages(decodedMessage.messages);
          break;
        default:
          return {error: 'Unknown message type'}
      }
    };

    this.websocket.onopen = () => {
      console.log('connected');
    };

    this.websocket.onclose = () => {
      console.log('connection lost');
      setTimeout(() => {this.start(token)}, 5000);

    }
  };

  componentDidMount() {
    if (!this.props.user) {
      this.props.history.push('/login')
    } else {
      const token = this.props.user.token;

      this.start(token);
    }
  };

  deleteMessageHandler = id => {
    const messageData = JSON.stringify({
      type: 'DELETE_MESSAGE',
      messageId: id
    });

    this.websocket.send(messageData);
  };

  messageChangeHandler = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  sendMessageHandler = event => {
    event.preventDefault();

    let message = JSON.stringify({
      type: 'CREATE_MESSAGE',
      message: this.state.message
    });

    this.websocket.send(message);
  };

  writePrivateMessageHandler = sender => {
    this.setState({sender, isPrivateMessage: true});
  };

  cancelWritePrivateMessageHandler = () => {
    if (this.state.isPrivateMessage) {
      this.setState({participantId: null, isPrivateMessage: false});
    }
  };

  sendPrivateMessageHandler = () => {
    const message = JSON.stringify({
      type: 'PRIVATE_MESSAGE',
      sender: this.props.user,
      message: this.state.message,
      participant: this.state.sender
    });

    this.websocket.send(message);
  };



  render() {
    return(
      <Fragment>
        <Row>
          <Col md={4}>
            <Panel>
              <Panel.Heading>Online users</Panel.Heading>
              <Panel.Body>
                {this.props.users.length > 0
                  && this.props.users.map(user => (
                    <p>{user.username}</p>
                  ))
                }
              </Panel.Body>
            </Panel>
          </Col>
          <Col md={8}>
            <Panel>
              <Panel.Heading>Chat room</Panel.Heading>
              <Panel.Body>
                <Col md={12}>
                  {this.props.messages.length > 0
                    && this.props.messages.map(message => (
                    message.participant
                      ? this.props.user.username === message.participant.username || this.props.user.username === message.sender.username
                          ? <Panel.Body key={message._id}>
                              <Col md={10}>
                                <span onClick={() => this.writePrivateMessageHandler(message.sender)}>
                                  Private message from <b>{message.sender.username}</b> to <b>{message.participant.username}</b>
                                </span>
                                <span style={{paddingRight: '10px'}}> {message.title}</span>
                              {this.state.isPrivateMessage && this.state.sender._id === message.sender._id
                                && <p> Private message to {message.sender.username}
                                     <Button onClick={this.cancelWritePrivateMessageHandler}>
                                       Cancel
                                     </Button>
                                   </p>
                              }
                              </Col>
                              {this.props.user.role === 'moderator'
                                && <Col md={2}>
                                      <Button onClick={() => this.deleteMessageHandler(message._id)}>
                                        Delete
                                      </Button>
                                   </Col>
                              }
                            </Panel.Body>
                          : null
                      : <Panel.Body key={message._id}>
                          <Col md={10}>
                            <span onClick={() => this.writePrivateMessageHandler(message.sender)}>
                              {message.sender.username} :
                            </span>
                            <span style={{paddingRight: '10px'}}> {message.title}</span>
                            {this.state.isPrivateMessage && this.state.sender._id === message.sender._id
                              && <p> Private message to {message.sender.username}
                                   <Button onClick={this.cancelWritePrivateMessageHandler}>
                                     Cancel
                                   </Button>
                                 </p>
                            }
                          </Col>
                          {this.props.user.role === 'moderator'
                            && <Col md={2}>
                                 <Button onClick={() => this.deleteMessageHandler(message._id)}>
                                   Delete
                                 </Button>
                               </Col>
                          }
                        </Panel.Body>
                    ))
                  }
                </Col>
                <Col md={10}>
                  <FormGroup>
                    <FormControl componentClass="textarea"
                                 placeholder={this.state.isPrivateMessage ? 'Type private message' : 'Enter message'}
                                 name='message'
                                 onChange={this.messageChangeHandler}/>
                  </FormGroup>
                </Col>
                <Col md={2}>
                  <Button onClick={this.state.isPrivateMessage ? this.sendPrivateMessageHandler : this.sendMessageHandler}>
                    Send
                  </Button>
                </Col>
              </Panel.Body>
            </Panel>
          </Col>
        </Row>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user,
  users: state.users.users,
  messages: state.messages.messages
});

const mapDispatchToProps = dispatch => ({
  onFetchNewMessage: message => dispatch(fetchNewMessage(message)),
  onFetchAllMessages: messages => dispatch(fetchAllMessages(messages)),
  onLoggedIn: (user) => dispatch(loggedIn(user)),
  onLoggedOut: (users) => dispatch(loggedOut(users)),
  onFetchConnectedUsers: users => dispatch(fetchConnectedUsers(users)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Chat)