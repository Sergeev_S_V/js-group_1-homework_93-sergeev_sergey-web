import React, {Fragment} from 'react';
import {NotificationContainer} from "react-notifications";
import 'react-notifications/lib/notifications.css';
import {Col} from "react-bootstrap";
import {connect} from "react-redux";
import {logoutUser} from "../../store/actions/users";
import Toolbar from "../../components/UI/Toolbar/Toolbar";

const Layout = ({user, children, logoutUser}) => (
  <Fragment>
    <NotificationContainer/>
    <header>
      <Toolbar user={user} logout={logoutUser}/>
    </header>
    <Col md={12}>
      <main className='container'>
        {children}
      </main>
    </Col>
  </Fragment>
);

const mapStateToProps = state => {
  return {
    user: state.users.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    logoutUser: () => dispatch(logoutUser())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
