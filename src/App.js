import React, { Component } from 'react';
import './App.css';
import Layout from "./containers/Layout/Layout";
import {Route, Switch} from "react-router";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import Chat from "./containers/Chat/Chat";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path='/register' exact component={Register}/>
          <Route path='/login' exact component={Login}/>
          <Route path='/chat' exact component={Chat}/>
          <Route path='/' exact component={Chat}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;