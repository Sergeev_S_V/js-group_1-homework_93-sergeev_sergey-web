import React from 'react';
import {Navbar} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import {connect} from "react-redux";
import AnonymousMenu from "../Menus/AnonymousMenu";
import UserMenu from "../Menus/UserMenu";

const Toolbar = ({user, logout}) => (
  <Navbar>
    <Navbar.Header>
      <Navbar.Brand>
        <LinkContainer to="/chat" exact>
          <a>Chat</a>
        </LinkContainer>
      </Navbar.Brand>
    </Navbar.Header>
    <Navbar.Collapse>
      {user
        ? <UserMenu logout={logout} user={user}/>
        : <AnonymousMenu/>
      }
    </Navbar.Collapse>
  </Navbar>
);

const mapStateToProps = state => ({
  user: state.users.user
});

export default connect(mapStateToProps)(Toolbar);