import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import {ConnectedRouter} from "react-router-redux";

import './index.css';
import App from './App';
import axios from './axios-api';
import registerServiceWorker from './registerServiceWorker';
import {history} from './store/configureStore';
import store from './store/configureStore';
import {logoutExpiredUser} from "./store/actions/users";

axios.interceptors.request.use(config => {
  try {
    config.headers['Token'] = store.getState().users.user.token;
  } catch (e) {
    // do nothing
  }

  return config;
});

axios.interceptors.response.use(response => response,
  error => {
    if (error.response.status === 401) {
      store.dispatch(logoutExpiredUser());
    }
  });

const app = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App/>
    </ConnectedRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
